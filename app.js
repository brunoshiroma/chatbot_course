"use strict";

const RtmClient = require('@slack/client').RtmClient;

const MemoryDataStore = require('@slack/client').MemoryDataStore;
const RTM_EVENTS = require('@slack/client').RTM_EVENTS;
const token = process.env.SLACK_TOKEN || 'xoxb-163034661031-09zY6K9yPj2TN71iJ3IAIlKx';

const rtm = new RtmClient(token, {
	logLevel : 'error',
	dataStore : new MemoryDataStore(),
	autoReconnect : true,
	autoMark : true
});



let state = null;

const data = {};

const setState = (newState) => {
	state  = newState;
}

const handlers = {};

const router = (message) => {
	if(!state){
		handlers.DEFAULT(message);
	} else {
		handlers[state](message);
	}

}

handlers.DEFAULT = (message) => {
	const user = rtm.dataStore.getUserById(message.user);
	console.log(user);
	rtm.sendMessage(`Welcome ${user.name}! what's your name ?`, message.channel);
	setState("GET_NAME");
}

handlers.GET_NAME = (message) => {
	data[message.user] = {
		name : message.text
	};
	rtm.sendMessage("What's your phone ?", message.channel)
	setState('GET_PHONE')
}

handlers.GET_PHONE = (message) =>{
	data[message.user].phone = message.text;
	setState('GET_ADDRESS');
	rtm.sendMessage("What's is your address ?", message.channel);
}

handlers.GET_ADDRESS = (message) => {
	data[message.user].address = message.text;
	rtm.sendMessage(`Confirm the data name : ${data[message.user].name} phone : ${data[message.user].phone} address : ${data[message.user].address} ?`, message.channel)
	setState('CONFIRM_DATA');
};

handlers.CONFIRM_DATA = (message) => {
	if(message.text === 'yes'){
		rtm.sendMessage('Thank you !', message.channel);
	} else {
		rtm.sendMessage("Ops, let's start again", message.channel);
	}
	data[message.user] = null;
	setState('DEFAULT');
}

rtm.on(RTM_EVENTS.MESSAGE, (message) => {
	if(message.channel === 'G79CLHEA3'){
		console.log('Bot sees message in general and chooses not to respond.');
	} else{
		router(message);
	}

});

rtm.start();